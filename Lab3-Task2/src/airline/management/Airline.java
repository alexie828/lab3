package airline.management;

import java.util.HashMap;
import java.util.Map;


public class Airline {
	private String name;
	private Map<Airplane,AirplaneFleetRecord> fleet;
	
	public Airline(String name) {
		this.name = name;
		this.fleet = new HashMap<Airplane,AirplaneFleetRecord>();
	}

	public int getNumberOfPlanes(Airplane airplane){
		//TODO	
		return 0;
	}
	
	public void addAirplaneFleetRecord(Airplane a) throws AirlineException{
		//TODO
	}
	public boolean isInTheFleet(Airplane a){
		//TODO
		return false;
	}
	public AirplaneFleetRecord getAirplaneFleetRecord(Airplane a){
		//TODO
		return null;
	}
	
	public void buyAirplane(Airplane a, int quantity) throws AirlineException {
		throw new AirlineException("unimplemented");
	}
	public void sellAirplane(Airplane a, int quantity) throws AirlineException {
		throw new AirlineException("unimplemented");
	}

}